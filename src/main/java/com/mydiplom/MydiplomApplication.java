package com.mydiplom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@SpringBootApplication
public class MydiplomApplication {

	public static void main(String[] args) {
		SpringApplication.run(MydiplomApplication.class, args);
	}

}
