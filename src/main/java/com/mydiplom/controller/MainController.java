package com.mydiplom.controller;

import com.mydiplom.domain.Claim;
import com.mydiplom.repository.ClaimRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@CrossOrigin
public class MainController {

    private final ClaimRepository claimRepository;

    public MainController(ClaimRepository claimRepository) {
        this.claimRepository = claimRepository;
    }

    @GetMapping("/greeting")
    public String greeting(
            @RequestParam(name="name", required=false, defaultValue="World") String name,
            Map<String, Object> model
    ) {
        model.put("name", name);
        return "greeting";
    }

    @GetMapping
    public String main(Map<String, Object> model) {
        List<Claim> claims = claimRepository.findAll().stream().sorted((a, b) -> (int) (a.getId() - b.getId())).collect(Collectors.toList());
        model.put("items", claims);
        return "main";
    }
}
