package com.mydiplom.controller;

import com.mydiplom.domain.Claim;
import com.mydiplom.repository.ClaimRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("claim")
@CrossOrigin
public class ClaimController {

    private final ClaimRepository claimRepository;

    public ClaimController(ClaimRepository claimRepository) {
        this.claimRepository = claimRepository;
    }

    @GetMapping
    public List<Claim> getAll() {
        return claimRepository.findAll().stream().sorted((a, b) -> (int) (b.getId() - a.getId())).collect(Collectors.toList());
    }

    @GetMapping("{id}")
    public Claim getOne(@PathVariable("id") Claim claim) {
       return claim;
    }

    @PostMapping
    public Claim add(@RequestBody Claim claim) {
        return claimRepository.save(claim);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Claim claim) {
        claimRepository.delete(claim);
    }

    @PutMapping("{id}")
    public Claim update(@PathVariable("id") Claim claimFromDb,
                        @RequestBody Claim claim) {

        BeanUtils.copyProperties(claim, claimFromDb, "id");
        return claimRepository.save(claimFromDb);
    }

}
