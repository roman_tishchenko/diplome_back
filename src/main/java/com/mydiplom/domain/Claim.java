package com.mydiplom.domain;

import lombok.Data;

import javax.persistence.*;
import java.sql.*;

@Entity
@Data
@Table
public class Claim {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    private Status status = Status.CREATED;

    private String fio;

    private String op;

    private int course;

    private String projectName;

    private String scientificDirector;

    private String contactNumber;

    private String contactEmail;

    private String description;

    private String checker;

    public Claim(Status status, String fio, String op, int course,
                 String projectName, String scientificDirector,
                 String contactNumber, String contactEmail, String description,
                 String checker) {
        this.status = status;
        this.fio = fio;
        this.op = op;
        this.course = course;
        this.projectName = projectName;
        this.scientificDirector = scientificDirector;
        this.contactNumber = contactNumber;
        this.contactEmail = contactEmail;
        this.description = description;
        this.checker = checker;
    }

    public Claim() {
    }

    public Claim updateInDb(Long id, String tag, String text) {
        if (id != null) {
            try(Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/mydiplom", "postgres", "1");
                Statement statement = connection.createStatement()) {

                ResultSet resultSet = statement.executeQuery("select * from item");
                while (resultSet.next()) {
                    System.out.println(resultSet.next());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            throw new IllegalArgumentException();
        }

        return null;
    }

}
