package com.mydiplom.domain;

public enum Status {
    CREATED, IN_WORK, COMPLETED, CANCELED
}
